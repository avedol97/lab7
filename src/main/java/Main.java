import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Main {
    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        Document document = new Document();
        @SuppressWarnings("unused")
        PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream("Resume.pdf"));
        document.open();
        document.add(Paragraph());
        document.add(Table());
        document.close();
    }

    private static Paragraph Paragraph(){
        Font largeBold = new Font(Font.FontFamily.TIMES_ROMAN, 26, Font.BOLD);
        Paragraph paragraph = new Paragraph("Resume", largeBold);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        paragraph.setSpacingAfter(80);
        return paragraph;
    }

    private static PdfPTable Table(){
        PdfPTable table = new PdfPTable(2);

        table.addCell("  First Name");
        table.addCell(" Krystian");
        table.addCell("  Last Name");
        table.addCell(" Cwioro");
        table.addCell("  Profession");
        table.addCell(" Student");
        table.addCell("  Education");
        table.addCell("  2018 - 2021 PWSZ in Tarnow");
        table.addCell("  Summary");
        table.addCell(" Państwowa Wyższa Szkoła Zawodowa w Tarnowie – publiczna uczelnia typu zawodowego, utworzona na podstawie Rozporządzenia Rady Ministrów z dnia 19 maja 1998 roku, j" +
                "ako pierwsza w kraju uczelnia zawodowa nowego typu kształcąca na poziomie licencjackim i inżynierskim");
        return table;
    }

}
